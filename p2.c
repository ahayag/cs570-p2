/* Program 2
 * Aaron Hayag 817898933
 * Dr. John Carroll
 * CS 570
 *
 * This program is a simple command line interpreter. It will continue to prompt for input until it sees an EOF marker
 * as the first word. The first word is treated as the name of an executable file and the words after are treated as
 * the arguments. It can handle redirection of IO with the metacharacters ">", "<", and ">&", as well as putting jobs
 * in the background if the "&" character is at the end of a line. Finally, it includes built in functions such as cd,
 * !!, and "done" (exits the program similar to p1) commands. All syntactic analysis is done in a function called 
 * parse() which also sets flags for metacharacters.
 */

#include "p2.h"

int wordCount; //word count in a line. "echo hi" = 2
int charCount; //character count a word has. "echo" = 4
char s[STORAGE]; //store words here when calling getword()
char *newargv[MAXARGS]; 
char *tempargv[MAXARGS]; //saves the previous input (will be used for !! command).
char *inoutfiles[2]; //file pointers
int lessFlag; //"<"
int greaterFlag; //">"
int amperFlag; //"&"
int bangFlag; //"!"

void myhandler(int signum){
	//printf("Received SIGTERM (%d), and the special handler is running...\n",signum);
}

int main(){
	char cd[] = "cd";
	char done[] = "done";
	char bangbang[] = "!!";
	char echo[] = "echo";
	int kidpid;
	int output_fd;
	int input_fd;
	int devnull_fd;
	int first = 0; //tracks where the "first" word of the command line. ie. "< fileB > fileA somecommand" first = 4.

	setpgid(0,0);
	signal(SIGTERM, myhandler);

	for(;;){	
		wordCount = 0;
		lessFlag = 0;
		greaterFlag = 0;
		amperFlag = 0;
		bangFlag = 0;

		printf("%%1%% ");
		
		//before calling parse we want to copy all the contents from newargv to tempargv in case the "!!" command is used.
		//second, we want to empty all the contents in newargv so that they don't show up in the next parse call.
		int j;
		for(j = 0; j < MAXARGS; j++){
			if(newargv[j] != NULL){
				tempargv[j] = (char*)malloc(60*strlen(newargv[j]+1));
				strcpy(tempargv[j],newargv[j]);
			}
			newargv[j] = NULL;
		}

		parse();

		if(wordCount == 0 && charCount == 0){ //input line with 0 words. skip and reissue prompt
			continue;
		}

		if(wordCount == 0 && charCount == -1){ //line with only EOF marker found. end program
			break;
		}

		if(strcmp(newargv[0],cd) == 0){ //cd is the first word
			if(wordCount == 1){ //cd with no arguments
				if(chdir(getenv("HOME")) != 0){ //use getenv to obtain value of HOME
					perror("cd to home directory failed");
				}
			}
			else if(wordCount == 2){ //cd with 1 argument
				if(chdir(newargv[1]) != 0){
					perror("cd to directory failed"); //No such file or directory exists.
				}
			}
			else{ //cd with 2+ arguments
				perror("cd failed. more than two arguments");
			}
			continue;
		}

		if(strcmp(newargv[0],done) == 0){ //done is first word. end program
			break;
		}

		//!! command. reinsert all the PREVIOUS content back to newargv which we store in tempargv before each parse().
		if(bangFlag != 0){ 
			if(strcmp(newargv[0],bangbang) == 0){
				int k;
				for(k = 0; k < MAXARGS; k++){
					newargv[k] = tempargv[k];
				}	
			}
		}
		
		//prevents IO redirection in case of input such as "echo hello<there"
		if(strcmp(newargv[0],echo) == 0){
			greaterFlag = 0;
			lessFlag = 0;
		}

		if(lessFlag == 2){ //lessFlag is incremented once when "<" is found. it is incremented again when an "infile" is
				   //found. this is why we check if it is == 2.
			int in_flags;
			in_flags = O_RDONLY;
			if((input_fd=open(inoutfiles[0],in_flags,S_IRUSR | S_IWUSR))<0){
				perror("Failed to open input file");
				continue;
			}
		}
		else if(lessFlag > 2){
			perror("Too many redirects ('<')");
			continue;
		}

		if(greaterFlag == 2){ //1 means char ">" is found, 2 means "outfile" is found
			int out_flags;
			out_flags = O_CREAT | O_WRONLY;
			if((access(inoutfiles[1],F_OK)) != -1){ //check to see if file already exists because we want to refuse to
								//overwrite existing files.
				perror("File already exists");
				continue;
			}
			else{
				if((output_fd=open(inoutfiles[1],out_flags,S_IRUSR | S_IWUSR))<0){
					perror("Failed to open output file");
					continue;
				}
			}
		}
		else if(greaterFlag > 2){
			perror("Too many redirects ('>')");
			continue;
		}

		if(strcmp(newargv[wordCount - 1],"&") == 0){ //the LAST word in the line is "&"
			newargv[wordCount - 1] = NULL; //set the last word to NULL because we don't want to pass it onto the child
			amperFlag++;
			int dev_flags;	//redirect stdin to /dev/null for background children
			dev_flags = O_RDONLY;
			if((devnull_fd=open("/dev/null",dev_flags,S_IRUSR | S_IWUSR))<0){
				perror("Failed to open /dev/null file");
				continue;
			}
		}

		//for cases where the FIRST word is not argv[0]. ie. < fileB > fileA somecommand
		//go through every argument in the command line and find the FIRST word which is the one that is not a
		//metacharacter or a input/output file.
		if((strcmp(newargv[0],"<") == 0) || (strcmp(newargv[0],">") == 0)){
			int i;
			for(i = 0; i < wordCount; i++){
				if(strcmp(newargv[i],"<") == 0){
				}
				else if(strcmp(newargv[i],">") == 0){
				}
				else if(strcmp(newargv[i],"&") == 0){
				}
				else if(strcmp(newargv[i],inoutfiles[0]) == 0){
				}
				else if(strcmp(newargv[i],inoutfiles[1]) == 0){
				}
				else{
					first = i;
					break;
				}		
			}
		}

		fflush(stdout); //ensures that children inherit only empty buffers
		fflush(stderr);

		if((kidpid = (int)fork()) == -1){
			perror("Cannot fork");
			exit(EXIT_FAILURE);
		}
		//child process. redirect I/O as requested. execute the file.
		else if(kidpid == 0){

			if(lessFlag == 2){
				//we use dup2 so that STDIN can be used as the file descriptor to our input file 
				if((dup2(input_fd,STDIN_FILENO)) < 0){
					perror("dup2 for input file and STDIN failed");
					exit(1);
				}
				//close the original input file descriptor as we will not be needing it
				if((close(input_fd) < 0)){
					perror("Failed to close input file");
					exit(2);
				}
		}

			if(greaterFlag == 2){	
				//we use dup2 so that STDOUT can be used as the file descriptor to our output file
				if((dup2(output_fd,STDOUT_FILENO)) < 0){
					perror("dup2 for outputfile and STDOUT failed");
					exit(3);
				}
				//close the original output file descriptor as we will not be need it
				if((close(output_fd) < 0)){
					perror("Failed to close output file");
					exit(4);
				}
			}
			
			if(amperFlag == 1){
				if((dup2(devnull_fd,STDIN_FILENO)) < 0){
					perror("dup2 for devnull and STDIN failed");
					exit(5);
				}
				if((close(devnull_fd) < 0)){
					perror("Failed to close devnull file");
					exit(6);
				}
			}

			if((execvp(newargv[first], newargv)) == -1){
				perror("Failed to execute(execvp)");
				exit(7);
			}
		}
		else{
			//if "&" is the LAST word, parent prints out PID and start a new process to execute the command
			if(amperFlag == 1){
				printf("%s [%d]\n",newargv[0],getpid());
			}	
			//else wait for child to complete
			else{
				for(;;){
					pid_t pid;
					CHK(pid = wait(NULL));
					if(pid == kidpid){
						break;
					}
				}
			}
		}	
	}
	killpg(getpgrp(),SIGTERM); //use killpg to terminate any children that are still running
	printf("p2 terminated.\n");
	exit(0);	
}

//all syntactic analysis is done here. when a metacharacter is found, its corresponding flag is incremented.
//we also use parse to point inoutfiles[1] to the outfile and inoutfiles[0] to the infile IF they exist.
//finally, newargv[MAXARGS] will contain each word in the input line.
int parse(){
	int i = 0;
	int sPointer = 0;

	while((charCount = getword(s + sPointer)) != 0){

		if(charCount == -1){ //EOF marker found 
			break;
		}

		newargv[i] = s + sPointer;

		if(greaterFlag == 1){ //greaterFlag has been incremented which means the NEXT word has to be the outfile
			inoutfiles[1] = s + sPointer; //inoutfiles[1] now points to the outfile
			greaterFlag++; //increment greaterflag again so we know that ">" is found AND an "outfile" is found.
		}
		if(lessFlag == 1){ //we repeat the above procedure for "<".
			inoutfiles[0] = s + sPointer;
			lessFlag++;
		}
		if(*(s+sPointer) == '>'){
			greaterFlag++;
		}
		if(*(s+sPointer) == '<'){
			lessFlag++;
		}
		if(*(s+sPointer) == '!'){
			bangFlag++;
		}

		i++;
		sPointer += charCount + 1;
		wordCount++;
	}
}

