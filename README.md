This is a continuation of assignment p1 from my operating systems class.. The goal is to write a simple command line interpreter using the 
getword() function written in p1.

 This program is a simple command line interpreter. It will continue to prompt for input until it sees an EOF marker
 as the first word. The first word is treated as the name of an executable file and the words after are treated as
 the arguments. It can handle redirection of IO with the metacharacters ">", "<", and ">&", as well as putting jobs
 in the background if the "&" character is at the end of a line. Finally, it includes built in functions such as cd,
 !!, and "done" (exits the program similar to p1) commands. All syntactic analysis is done in a function called 
 parse() which also sets flags for metacharacters.
