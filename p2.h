#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "getword.h"
#include <signal.h>
#include <sys/types.h>
#include "CHK.h"
#define MAXITEM 100 /* max number of words per line */
#define MAXARGS 100
#define STORAGE 255 //buffer for getword() function
int parse();
