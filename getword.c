/* Program 1
 * Aaron Hayag 817898933
 * Dr. John Carroll
 * CS 570
 *
 * The getword() function reads a word from the input stream and outputs the number of characters in the word.
 * It also takes a pointer to the beginning of a char array and assembles a "word" as it reads each characters.
 * Exceptions are whitespaces, which are ignored, newline, which returns 0, EOF, which returns -1, and the string "done" which returns -1.
 * Metacharacters "<", ">", ">&", ">>", ">>&", "|", "#", and "&" are treated as delimiters and are output as is (collection of these metachars is greedy).
 * If a backslash character preceds a metacharacter or a space, they will be treated as a regular character. Otherwise backslashes are ignored.
 * Finally, if a  word scanned is longer than 254 characters, the function outputs a string consisting consisting of these characters. The remaining parts
 * of the word is used in a new  getword() call.
 */

#include "getword.h"

int getword(char *w){
	int iochar;
	int count;
	int metaCount; //a separate counter for metacharacters (that can range from 1 to 3).
	               //to easily differentiate from the "normal" character counter in the code.
	int i = 0;
	char done[] = "done";

	iochar = getchar(); //a single getchar() is added here to catch and handle any characters
	                    //that were passed through ungetc() from the while loop below.

	while(iochar == ' '){ //this will ignore all the LEADING whitespaces
		iochar = getchar();
	}

	if(iochar == EOF){
		*w = '\0';
		return -1;
	}
	else if(iochar == '\n'){
		*w = '\0';
		return 0;
	}
	else if(iochar == '<' || iochar == '|' || iochar == '#' || iochar == '&'){ //check for single count metacharacters. return 1 if found and add to string
		*w = iochar;
		i++;
		while(*(w+i) != '\0'){ //make sure that the character array does not contain characters from 
				       //previous getword() calls. erase the characters if so.
			*(w+i) = '\0';
			i++;
		}
		return 1;
	}
	else if(iochar == '>'){ //special case if '>' encountered.
		iochar = getchar(); //we must get the next char to check if '>&', '>>' or '>>&' possible.
		if(iochar == '&'){ //'>&' found. store in character string.
			metaCount = 2;
			*w = '>';
			i++;
			*(w+i) = '&';
			i++;
		}
		else if(iochar == '>'){ //we have '>>' so far.
			iochar = getchar(); //must get next char to see if '>>&' is still  possible.
			if(iochar != '&'){ //this means '>>&' is not possible
				metaCount = 2;
				ungetc(iochar,stdin); //we call ungetc() because we just want to store '>>' for now. we put back this next char back into the 
						      //input stream because we will use it in the next getword() call instead. 
				*w = '>';
				i++;
				*(w+i) = '>';
				i++;
			}
			else{ //'>>&' found. store in character string.
				metaCount = 3;
				*w = '>';
				i++;
				*(w+i) = '>';
				i++;
				*(w+i) = '&';
				i++;
			}
		}
		else{ //only '>' is found. 
			metaCount = 1;
			ungetc(iochar,stdin); //we don't want to use the nextchar yet. we store just '>' for now but  save the nextchar for next call  using ungetc()
			*w = '>';
			i++;
		}
		while(*(w+i) != '\0'){ //same while loop as before to make sure character string only contains the metachars found.
			*(w+i) = '\0';
			i++;
		}
		return metaCount;
	}
	else if(iochar == '\\'){ //backslash found. get and store the next char in string bypassing all exceptions (whitespace and metacharacters).
		iochar = getchar();
		*(w+i) = iochar;
		i++;
	}
	else{ //character is "normal", store in string and proceed as usual.
		*(w+i) = iochar;
		i++;
	}

	while((iochar = getchar()) != EOF){ 
		if(iochar == ' '){ //if a whitespace is found enter a while loop that will keep grabbing the next char
				   //so long as it is a whitespace. this will handle all TRAILING whitespaces or any
				   //whitespaces IN BETWEEN words.
			while((iochar = getchar()) == ' '){
			}
			ungetc(iochar,stdin); //this next char is not a whitespace. use ungetc() to put it back input stream
					      //because we will need it for the next getword() call.
			break; //break out of loop to return the string collected so far
		}
		else if(iochar == '<' || iochar == '|' || iochar == '#' || iochar == '&'){ //metacharacter found
			ungetc(iochar,stdin); //put back into input stream using ungetc()
			break; //break to return string collected so far
		}
		else if(iochar == '>'){ //metacharacter found. same procedure as above
			ungetc(iochar,stdin);
			break;
		}
		else if(iochar == '\n'){ //newline found. same procedure as above
			ungetc('\n',stdin);
			break;
		}
		else if(iochar == '\\'){ //backslash means we have to get the next char regardless if it is a metachar or whitespace and store in the string
			iochar = getchar();
			if(iochar == '\n'){ //only exception is that a backslash doesn't affect newlines
				ungetc('\n',stdin); //ungetc() the new line so the next getword() call can handle and correctly output it
				break;
			}
			*(w+i) = iochar;
			i++;
		}
		else{ //normal character is found. simply add it to the character string
			*(w+i) = iochar;		
			i++;
			if(i == 254){ //if the character counter == 254 we have to break out of the loop and output the collected string so far 
                                      //this is because STORAGE - 1 is 254
				break;
			}
		}
	}

	count = i;
	
	while(*(w+i) != '\0' || *(w+i) == ' '){ //remove any remaining characters collected from a previous getword() call
		*(w+i) = '\0';
		i++;
	}

	if(strcmp(w,done) == 0){ //if the word collected is "done" we must return -1 as specified in the specs.
		return -1;
	}

	return count;
}
